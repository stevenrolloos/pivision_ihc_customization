# PIVision_Extensibility



***** OVERALL ******

This bucket contains all the necessary files to enhance PIVision to the IHC PIVision dashboard. It contains 3 things:

1) Change of PIvision to IHC logo

2) Addition of the Box shape to the menu

3) Several extensions to show customized graphics in the PIVision dashboard

The main location of all files on the PIVision server is: 

C:\Program Files\PIPC\PIVision\

**** 1) IHC LOGO INSTEAD OF PIVision logo ****

To change the PIVision logo in the IHC Logo, we need to change 2 files:

PIVisualization.pv-header-directive.js in Scripts/app/editor/layout
pv-header-template.html in Scripts/app/editor/layout

**** 2) BOX SHAPE ****

To add the box shape to the PIVision menu, the follow files need to be changed:

PIVisualization.sym-shape.js in Scripts\app\editor\symbols\
sym-shape-config.html in Scripts\app\editor\symbols\
sym-box-template.html in Scripts\app\editor\symbols\ext\
toolbar-shape-menu-template.html in Scripts\app\editor\display

**** 3) EXTENSIONS ****

*** LOCATION OF EXTENSIONS ***

C:\Program Files\PIPC\PIVision\Scripts\app\editor\symbols\ext

*** EXTESIONS SPECIFIC ***

Extension consists of a combination of files:

1) JavaScript (.js) file - containing the logic of what is happening

2) ...config.html file - containing the menu when right click -> config is done on the object

3) ...template.html - containing the visual template of the the object 

4) .png icon (in folder /icons) - which is the icon you choose to select the object


*** PER EXTENSION ***

*** amcharts-pie ***
Description: Pie chart based on amcharts pie graph
Inputs: Any value you want, shows values in chart in order of putting in the tags
Output: Pie chart

*** barchartcompare ***
Description: Barchart based on amcharts bar chart
Inputs: Any value you want, shows values in chart in order of putting in the tags
Output: Bar chart

*** box ***
Description: Box with textfield on top of it. Basic color is Grey
Inputs: None
Output: Box with textfield on top

*** customtable2 ***
Description: Alarm table
Inputs: Alarm I/Os: zeros and ones
Output: Table with Trend and calculation of minutes on during selected time period

*** customtext ***
Description: IHC font custom text
Inputs: None - write own text 
Output: the same as the textbox symbol but then it is possible to make text bold and change font to IHC font

*** customvalue ***
Description: value symbol in IHC font and bold
Inputs: any value
Output: the same as the value symbol but then it is possible to make text bold and change font to IHC font

*** imageAFurl ***
Description: Image is searched from Images folder to represent client logo 
Inputs: Client name tags
Output: Logo of the client (if this is in the PIVision images folder)

*** olmap ***
Description: Map of current position of vessels
Inputs: Latitude,Longitude
Output: Map with current position
Notes: If Latitude and Longitude are non-existent but are added, the entire map will fail

*** olmap2 ***
Description: Map of dredge position - last 5 known positions
Inputs: Latitude, Longitude
Output: Map with the last 5 positions and lines between these points for a particular asset
Notes: If Latitude and Longitude are non-existent but are added, the entire map will fail

