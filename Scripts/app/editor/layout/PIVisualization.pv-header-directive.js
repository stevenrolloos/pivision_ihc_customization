﻿// <copyright file="PIVisualization.pv-header-directive.js" company="OSIsoft, LLC">
// Copyright © 2014-2017 OSIsoft, LLC. All rights reserved.
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF OSIsoft, LLC.
// USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR EXPRESS WRITTEN
// PERMISSION OF OSIsoft, LLC.
//
// RESTRICTED RIGHTS LEGEND
// Use, duplication, or disclosure by the Government is subject to restrictions
// as set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and
// Computer Software clause at DFARS 252.227.7013
//
// OSIsoft, LLC.
// 1600 Alvarado Street, San Leandro, CA 94577
// </copyright>

/// <reference path="../_references.js" chutzpah-exclude="true" />

window.PIVisualization = window.PIVisualization || {};

(function (PV) {
    'use strict';

    angular.module(APPNAME)
        .directive('pvHeader', pvHeader);

    pvHeader.$inject = ['contextPath', '$window'];

    function pvHeader(contextPath, $window) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: contextPath + 'scripts/app/editor/layout/pv-header-template.html',

            link: function (scope) {
                // read the 2 letter lang attribute off the HTML tag
                var lang = $('html').attr('lang');

                // client resource strings
                scope.res = PV.ResourceStrings || {};
                scope.helpMenuItems = [
                    { id: 'HelpMenuOffLine', title: PV.ResourceStrings.HelpMenuOffLine, url: contextPath + 'Help/?page=GUID-F3B18F98-0C47-4EDF-9C9A-E4C711A3405B.html' },
                    { id: 'HelpMenuOnLine', title: PV.ResourceStrings.HelpMenuOnLine, url: 'https://livelibrary.osisoft.com/LiveLibrary/content/en/vision-v1/GUID-F3B18F98-0C47-4EDF-9C9A-E4C711A3405B ' },
                    { id: 'HelpMenuTrainingVideos', title: PV.ResourceStrings.HelpMenuTrainingVideos, url: 'https://www.youtube.com/playlist?list=PLMcG1Hs2JbcvWPkSbIbQEJqsTX9Sa1nty' },
                    { id: 'HelpMenuUserVoice', title: PV.ResourceStrings.HelpMenuUserVoice, url: null },
                    { id: 'HelpMenuUserVoiceSmartvote', title: PV.ResourceStrings.HelpMenuUserVoiceSmartvote, url: null }];

                scope.onHelpMenu = function (e) {
                    var item = $(e.item);
                    switch (item.attr('data-menu-id')) {
                        case 'HelpMenuUserVoice':
                            showUserVoice();
                            break;
                        case 'HelpMenuUserVoiceSmartvote':
                            showUserVoice('smartvote');
                            break;
                        default:                            
                            if (item.attr('data-url')) {
                                openLink(item);
                            }
                    }
                };

                scope.getHelp = function() {
                    window.open('https://www.royalihc.com/en/services/operate-and-consultancy/24-7-support');
                }

                scope.onOpenHelpMenu = function () {
                    $('#help-context-menu').data('kendoContextMenu').enable('li[data-menu-id="HelpMenuUserVoice"]', !Array.isArray(UserVoice));
                    $('#help-context-menu').data('kendoContextMenu').enable('li[data-menu-id="HelpMenuUserVoiceSmartvote"]', !Array.isArray(UserVoice));
                };

                function showUserVoice(userVoiceMode) {
                    window.UserVoice.push(['set', {
                        forum_id: 320517,
                        menu_enabled: true,
                        smartvote_enabled: true,
                        post_idea_enabled: true,
                        satisfaction_enabled: false,
                        contact_enabled: false
                    }]);

                    window.UserVoice.push(['show', { mode: userVoiceMode || 'post_suggestion', target: '#help-button', locale: lang }]);
                }

                function openLink(item) {
                    $window.open(item.attr('data-url'), '_blank');
                }
            }
        };
    }
})(window.PIVisualization);
