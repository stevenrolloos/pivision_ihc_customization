﻿// <copyright file="PIVisualization.sym-shape.js" company="OSIsoft, LLC">
// Copyright © 2014-2017 OSIsoft, LLC. All rights reserved.
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF OSIsoft, LLC.
// USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR EXPRESS WRITTEN
// PERMISSION OF OSIsoft, LLC.
//
// RESTRICTED RIGHTS LEGEND
// Use, duplication, or disclosure by the Government is subject to restrictions
// as set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and
// Computer Software clause at DFARS 252.227.7013
//
// OSIsoft, LLC.
// 1600 Alvarado Street, San Leandro, CA 94577
// </copyright>

/// <reference path="../_references.js" />

window.PIVisualization = window.PIVisualization || {};

(function (PV) {
    'use strict';

    function shapeVis() { }
    PV.deriveVisualizationFromBase(shapeVis);

    shapeVis.prototype.init = function (scope, elem) {
        this.onConfigChange = configChange;
        this.onResize = resize;
        this.onPathUpdate = pathUpdate;
        
        this.onDataUpdate = function(data) {
            // if (data == null) {
            //     return;
            // };
            // if (scope.config.BoxItems.chosenLabel === 'value') {
            //     scope.config.HeaderText = data.Value;
            // } else if (scope.config.BoxItems.chosenLabel === 'label') {
            //     scope.config.HeaderText = data.Label || scope.config.HeaderText;
            // } else if (scope.config.BoxItems.chosenLabel === 'customtext') {
            //     scope.config.HeaderText = scope.config.BoxItems.customText;
            // } else {
            //     scope.config.HeaderText = data.Label.split('|')[scope.config.BoxItems.chosenLabel.id] || scope.config.HeaderText;
            // }
            // if (data.Label) {
            //     if (data.Label != scope.config.BoxItems.label) scope.config.BoxItems.chosenLabel = undefined;
            //     // scope.config.BoxItems.value = data.Value;
            //     // scope.config.BoxItems.label = data.Label;
            //     var parts = data.Label.split('|');
            //     parts.forEach((p, i) => {
            //         scope.config.BoxItems.options.push({id: i, label: p});
            //     })
            //     scope.config.BoxItems.labelParts.push({id: scope.config.BoxItems.labelParts.length - 1, label: data.Label});
            //     scope.config.BoxItems.labelParts.push({id: scope.config.BoxItems.labelParts.length - 1, label: data.Value});
            // }
            // console.log(scope.config.BoxItems);
        }

        this.onDatasourceChange = function(...data) {
        }

        Object.defineProperty(scope, 'fillColor', {
            get: function () {
                return scope.Fill || scope.config.Fill;
            }
        });

        Object.defineProperty(scope, 'arrowOffset', {
            get: function () {
                var OFFSET = 14,
                    ASPECT_RATIO = 0.55;
                return {
                    X: scope.config.StrokeWidth ? OFFSET + Number(scope.config.StrokeWidth) : OFFSET,
                    Y: scope.config.StrokeWidth ? ASPECT_RATIO * (OFFSET + Number(scope.config.StrokeWidth)) :
                            ASPECT_RATIO * OFFSET
                };
            }
        });

        Object.defineProperty(scope, 'linePoints', {
            get: function () {
                if (scope.symbol.SymbolType !== 'line' || 
                    (scope.symbol.SymbolType === 'line' && scope.config.Arrows &&
                    scope.config.Arrows.Type === 0)) {
                    return scope.points;
                }

                var arrowAngle = Math.atan2(scope.arrowOffset.Y, scope.arrowOffset.X);
                var offset = (scope.config.StrokeWidth / 2) / Math.sin(arrowAngle);
                var selectionBoxWidth = Math.abs(scope.points[1].X - scope.points[0].X);
                var selectionBoxHeight = Math.abs(scope.points[1].Y - scope.points[0].Y);
                var diagonalLine = Math.sqrt(Math.pow(selectionBoxWidth, 2) + Math.pow(selectionBoxHeight, 2));
                var minArrowLength = 0;
                var angle, deltaX, deltaY, startX, startY, endX, endY;

                if (scope.config.Arrows.Start === true) {
                    minArrowLength += scope.arrowOffset.X + offset;
                    if (scope.config.Arrows.Type === 2) {
                        minArrowLength += scope.config.StrokeWidth / 2;
                    }
                }

                if (scope.config.Arrows.End === true) {
                    minArrowLength += scope.arrowOffset.X + offset;
                    if (scope.config.Arrows.Type === 2) {
                        minArrowLength += scope.config.StrokeWidth / 2;
                    }
                }

                if (diagonalLine < minArrowLength) { 
                    scope.runtimeData.ArrowDisabled = true;
                    return scope.points;
                }

                scope.runtimeData.ArrowDisabled = false;
                angle = Math.atan2(selectionBoxHeight, selectionBoxWidth);
                deltaY = offset * Math.sin(angle);
                deltaX = offset * Math.cos(angle);

                if ((scope.points[1].X > scope.points[0].X && scope.points[1].Y < scope.points[0].Y) ||
                    (scope.points[1].X < scope.points[0].X && scope.points[1].Y < scope.points[0].Y)) {
                    deltaY = -deltaY;
                }

                if ((scope.points[1].X < scope.points[0].X && scope.points[1].Y < scope.points[0].Y) || 
                    (scope.points[1].X < scope.points[0].X && scope.points[1].Y > scope.points[0].Y)) {
                    deltaX = -deltaX;    
                }

                startX = scope.config.Arrows.Start === true ? scope.points[0].X + deltaX : scope.points[0].X;
                startY = scope.config.Arrows.Start === true ? scope.points[0].Y + deltaY : scope.points[0].Y;
                endX = scope.config.Arrows.End === true ? scope.points[1].X - deltaX : scope.points[1].X;
                endY = scope.config.Arrows.End === true ? scope.points[1].Y - deltaY : scope.points[1].Y;

                return [{
                    X: startX, 
                    Y: startY 
                }, { 
                    X: endX, 
                    Y: endY 
                }];
            }
        });
        
        scope.points = [{
            _x: scope.config.Points && scope.config.Points.length > 0 ? scope.config.Points[0].X : 0,
            _y: scope.config.Points && scope.config.Points.length > 0 ? scope.config.Points[0].Y : 0,
            get X() {
                if (scope.position && scope.position.syncConfig) {
                    this._x = scope.config.Points[0].X;
                }

                return this._x;
            },
            set X(value) {
                this._x = value;
            },
            get Y() {
                if (scope.position && scope.position.syncConfig) {
                    this._y = scope.config.Points[0].Y;
                }

                return this._y;
            },
            set Y(value) {
                this._y = value;
            },
        },{
            _x: scope.config.Points && scope.config.Points.length > 1 ? scope.config.Points[1].X : 0,
            _y: scope.config.Points && scope.config.Points.length > 1 ? scope.config.Points[1].Y : 0,
            get X() {
                if (scope.position && scope.position.syncConfig) {
                    this._x = scope.config.Points[1].X;
                } 

                return this._x;
            },
            set X(value) {
                this._x = value;
            },
            get Y() {
                if (scope.position && scope.position.syncConfig) {
                    this._y = scope.config.Points[1].Y;
                }

                return this._y;
            },
            set Y(value) {
                this._y = value;
            },
        }];

        function configChange(newVal, oldVal) {
            // if (newVal.BoxItems) {
            //     if (newVal.BoxItems.customText !== '') {
            //         config.HeaderText = newVal.BoxItems.customText;
            //         config.BoxItems.chosenLabel = newVal.BoxItems.customText;
            //     }
            // }
            if (scope.shapeModel) {
                scope.shapeModel.updateConfig(newVal, oldVal);
                pathUpdate();
            } else {
                scope.shapeModel = new PV.SVGShapeModel(scope.config.Width, scope.config.Height, scope.config);
                if (scope.symbol.SymbolType === 'line') {
                    pathUpdate();
                    scope.runtimeData.StartArrowAngle = 0;
                    scope.runtimeData.EndArrowAngle = 0;
                } else if (scope.symbol.SymbolType === 'polygon') {
                    scope.shapeModel.Points = [];
                }
            }
        }

        function pathUpdate() {
            updatePolygon();
            // Line: scope.config.Points
            // Polygon: scope.shapeModel.Points
            if (scope.config.Points || scope.shapeModel.Points) {
                var path = '';
                var points = scope.config.Points || scope.shapeModel.Points;

                points.forEach(function (point) {
                    path += point.X + ',' + point.Y + ' ';
                });

                scope.shapeModel.Path = path;
                updateLineArrow();
            }

            if (scope.symbol.SymbolType === 'arc') {
                scope.runtimeData.def.updateArcPath(scope.shapeModel, scope.shapeModel.Quadrant, scope.runtimeData.position.height, scope.runtimeData.position.width, Math.ceil(scope.shapeModel.StrokeWidth / 2));
            }
        }

        function resize() {
            if (scope.shapeModel) {
                if (scope.symbol.SymbolType !== 'line') {
                    scope.shapeModel.setDimensions(scope.config.Width, scope.config.Height);
                    pathUpdate();
                } else {
                    updateLineArrow();
                }
            }
        }

        function updateLineArrow() {
            if (scope.symbol.SymbolType === 'line') {
                scope.runtimeData.StartArrowAngle = (Math.atan2(scope.linePoints[1].Y - scope.linePoints[0].Y, scope.linePoints[1].X - scope.linePoints[0].X) * 180 / Math.PI);
                scope.runtimeData.EndArrowAngle = (Math.atan2(scope.linePoints[1].Y - scope.linePoints[0].Y, scope.linePoints[1].X - scope.linePoints[0].X) * 180 / Math.PI);
            }
        }

        function updatePolygon() {
            if (scope.symbol.SymbolType === 'polygon') {
                var points = getPolygonPoints(scope.config.Sides, scope.position.width, scope.position.height);
                if (points) {
                    scope.shapeModel.Points = points;
                }
            }
        }

        function getPolygonPoints(sides, width, height) {
            if (sides > 2 && sides <= 50) {
                var points = [];
                var irregularFactor = width > height ? height / width : width / height;
                irregularFactor = isNaN(irregularFactor) ? 1 : irregularFactor;
                
                var strokeWidthCompensation = scope.config.StrokeWidth / Math.cos(Math.PI / sides * irregularFactor);
                for (var i = 0; i < sides; i++) {
                    var angle = 2 * Math.PI * i / sides;
                    var x = (width / 2) + (width - strokeWidthCompensation) / 2 * Math.sin(angle);
                    var y = (height / 2) - (height - strokeWidthCompensation) / 2 * Math.cos(angle);
                    points.push({
                        X: x,
                        Y: y
                    });
                }
                return points;
            }
        }

        elem.on('click', function (event) {
            event.stopPropagation();
        });
    };

    var defs = [{
        typeName: 'box',
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Single,
        createSymbolOnDataSourceDrop: false,
        getDefaultConfig: function () {
            var config = PV.SymValueLabelOptions.getDefaultConfig({
                Height: 0,
                Width: 0,
                Fill: 'rgba(150,143,133,1)',
                Rotation: 0,
                Stroke: 'rgba(255,255,255,1)',
                StrokeWidth: 2,
                StrokeStyle: '',
                HeaderText: '',
                Bold: false,
                IHCFont: false,
                DataShape: 'Value',
                BoxItems: { chosenLabel: 'customtext', customText: '', fullLabel: undefined, options: ['']  }
            });
            return config;
        },
        supportsCollections: true,
        themes: { reverse: { Fill: 'rgba(150,143,133,1)' } },
        templateUrl: 'scripts/app/editor/symbols/ext/sym-box-template.html'
    }, {
        typeName: 'rectangle',
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.None,
        createSymbolOnDataSourceDrop: true,
        getDefaultConfig: function () {
            return {
                Height: 0,
                Width: 0,
                Fill: 'rgba(255,255,255,1)',
                Rotation: 0,
                Stroke: 'rgba(255,255,255,1)',
                StrokeWidth: 3,
                StrokeStyle: ''
            };
        },
        templateUrl: 'scripts/app/editor/symbols/sym-rect-template.html'
    }, {
        typeName: 'ellipse',
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.None,
        createSymbolOnDataSourceDrop: true,
        getDefaultConfig: function () {
            return {
                Height: 0,
                Width: 0,
                Fill: 'rgba(255,255,255,1)',
                Rotation: 0,
                Stroke: 'rgba(255,255,255,1)',
                StrokeWidth: 3,
                StrokeStyle: ''
            };
        }
    }, {
        typeName: 'arc',
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.None,
        createSymbolOnDataSourceDrop: true,
        getDefaultConfig: function () {
            return {
                Quadrant: 1, 
                Sweep: 1,
                StartX: 0,
                StartY: 5,
                EndX: 75,
                EndY: 75,
                fStartX: 75,
                fStartY: 80,
                Height: 0,
                Width: 0,
                Fill: 'rgba(255,255,255,0)',
                Rotation: 0,
                Stroke: 'rgba(255,255,255,1)',
                StrokeWidth: 3,
                StrokeStyle: ''
            };
        },
        updateArcPath: function (model, quadrant, height, width, offset) {
            var sweep = 0,                
                startX = 0,
                startY = 0,
                endX = 0,
                endY = 0,
                fStartX = 0,
                fStartY = 0;
            
            switch (quadrant) {
                case 1:
                    sweep = 1;
                    startY = offset;
                    endX = width - offset;
                    endY = height;
                    fStartY = height;
                    break;
                case 2:
                    sweep = 1;
                    startX = width - offset;
                    endY = height - offset;
                    break;
                case 3:
                    startX = width;
                    startY = offset;
                    endX = offset;
                    endY = height;
                    fStartX = width;
                    fStartY = height;
                    break;
                case 4:
                    startX = offset;
                    endX = width;
                    endY = height - offset;
                    fStartX = width;
                    break;                
            }

            model.Quadrant = quadrant;
            model.Sweep = sweep;
            model.StartX = startX;
            model.StartY = startY;
            model.EndX = endX;
            model.EndY = endY;
            model.fStartX = fStartX;
            model.fStartY = fStartY;
        }
    }, {
        typeName: 'line',
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.None,
        createSymbolOnDataSourceDrop: true,
        getDefaultConfig: function () {
            return {
                Arrows: {
                    Type: 0,
                    Start: false,
                    End: false
                },
                Height: 0,
                Width: 0,
                Points: [{ X: 0, Y: 0 }, { X: 0, Y: 0 }],
                Rotation: 0,
                Fill: 'rgba(255,255,255,1)',
                StrokeWidth: 3,
                StrokeStyle: ''
            };
        },
        themes: { reverse: { Fill: 'black' } },
        configInit: PV.LineConfig.init,
        configTitle: PV.ResourceStrings.FormatLineOption,
        useCustomSelector: true
    }, {
        typeName: 'polygon',
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.None,
        createSymbolOnDataSourceDrop: true,
        getDefaultConfig: function () {
            return {
                Height: 0,
                Width: 0,
                Sides: 5,
                Rotation: 0,
                Fill: 'rgba(255,255,255,1)',
                Stroke: 'rgba(255,255,255,1)',
                StrokeWidth: 3,
                StrokeStyle: ''
            };
        },
        resizerMode: ''
    }];

    defs.forEach(function (def) {
        // shared configuration settings
        def.StateVariables = ['Fill', 'Blink'];
        def.visObjectType = shapeVis;
        def.supportsCollections = true;
        def.templateUrl = def.templateUrl ||'scripts/app/editor/symbols/sym-' + def.typeName + '-template.html';
        def.themes = def.themes || { reverse: { Stroke: 'red' } };
        def.configTitle = def.configTitle || PV.ResourceStrings.FormatShapeOption;
        def.configTemplateUrl = 'scripts/app/editor/symbols/sym-shape-config.html';
        def.loadConfig = function (config) {
            // PV 2017 and before, StrokeWidth configuration had no step and input[range] converted property to string
            // PV 2017 R2: Convert to numeric, round to step
            config.StrokeWidth = PV.symbolCatalog.adjustSliderProperty(config.StrokeWidth, 0.25);
            return true;
        };
        PV.symbolCatalog.register(def);
    });

})(window.PIVisualization);
