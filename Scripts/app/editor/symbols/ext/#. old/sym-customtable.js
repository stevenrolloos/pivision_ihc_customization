(function (PV){
    function tableVis() { }
    PV.deriveVisualizationFromBase(tableVis);

    tableVis.prototype.init = function (scope, elem, $rootScope) {
        let container = document.getElementById("temp-id");
        let newId = 'custom-table-' + Math.random().toString(36).substr(2, 16);
        container.id = newId;

        // Local data store so we always have labels
        let localData = [];
        scope.items = [];

        this.onDataUpdate = dataUpdate;

        function dataUpdate(data) {
            if (data === null) return;
            if (data.Data === undefined) return;

            // Copy the data into our local store.
            data.Data.forEach((d, idx) => {
                if (localData[idx] === undefined) {
                    // Initialize
                    localData[idx] = { Label: d.Label, Value: d.Values[0].Value, Time: d.Values[0].Time };
                } else {
                    // Update
                    localData[idx].Label = d.Label || localData[idx].Label;
                    localData[idx].Value = d.Values[0].Value;
                    localData[idx].Time = d.Values[0].Time;
                }
            });

            // Create view objects
            scope.items = localData.filter(d => d.Label.split('|').length === 3)
                .map(d => {
                    const cutLabel = d.Label.split('|');
                    const asset = cutLabel[0];
                    const category = cutLabel[1];
                    const name = cutLabel[2];

                    const onOff = parseInt(d.Value);

                    // Find a matching timeactive symbol if any
                    const listOfTimeActiveValues = localData.filter(d => d.Label.split('|').length === 4);

                    const timeActive = listOfTimeActiveValues.find(d => d.Label.split('|')[0] == asset && d.Label.split('|')[1] == category && d.Label.split('|')[2] == name) || {Value:'N/A'};

                    const result = {
                        name: name.split(' - ')[1],
                        category: category.split(':')[1],
                        onOff: onOff,
                        timeActive: timeActive.Value,
                        show: onOff === 1 || timeActive.Value > 0
                    };
                    return result;
                })
                .sort((b, a) => {
                    if (scope.config.DoubleSort) {
                        return a.onOff - b.onOff || a.timeActive - b.timeActive;
                    }
                    return a.timeActive - b.timeActive;
                }); // First sort by onoff, then by timeactive
            setTimeout(updateRows, 0);
        }

        function updateRows() {
            let rows = container.children;

            // Alternating row colors
            for (let i = 0; i < rows.length; i++) {
                if (i % 2 == 0) rows.item(i).classList.add('customodd');
                if (i % 2 == 1) rows.item(i).classList.remove('customodd');
                // if (config.ShowAll) rows.item(i).classList.add('override');
                // else rows.item(i).classList.remove('override');
            }
        }
        
        function dateParse(d) {
            var splitDate = d.split(" ");
            var date = splitDate[0];
            var day = date.split("-")[0];
            var month = date.split("-")[1];
            var year = date.split("-")[2];
  
            var time = splitDate[1];
            var hour = time.split(":")[0];
            var minute = time.split(":")[1];
            var seconds = time.split(":")[2];
  
            return new Date(Date.UTC(year, month - 1, day, hour, minute, seconds));
          }
    };
    
    var def = {
        typeName: "customtable",
        iconUrl: '/Scripts/app/editor/symbols/ext/Icons/customtable.png',
        displayName: "Custom table",
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Multiple,
        getDefaultConfig: function() {
            var config = PV.SymValueLabelOptions.getDefaultConfig({
                DataShape: 'TimeSeries',
                Height: 300,
                Width: 300,
                NameWidth: 60,
                CatWidth: 30,
                OnOffWidth: 5,
                TimeWidth: 5,
                ShowAll: false,
                DoubleSort: true,
                DataQueryMode: PV.Extensibility.Enums.DataQueryMode.ModeSingleton
            });
            return config;
        },
        configTitle: "Table config",
        visObjectType: tableVis
    };

    PV.symbolCatalog.register(def);
})(window.PIVisualization);