(function (PV) {
    function chartVis() { }
    PV.deriveVisualizationFromBase(chartVis);

    chartVis.prototype.init = function(scope, elem, rootScope) {
        // console.log('Initialized!', scope, elem, rootScope);

        this.onDataUpdate = dataUpdate;
        this.onDatasourceChange = datasourceChange;

        var localData = [];

        var div = document.getElementById("chartdiv");
        div.id = Math.random().toString(36).substr(2, 16);

        var chart = AmCharts.makeChart(div, 
        {
            "type": "serial",
            "categoryField": "attribute",
            "startDuration": 1,
            "addClassNames": true,
            "categoryAxis": {
                "gridPosition": "start",
                "gridThickness": 0
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "[[title]] of [[category]]:[[value]]",
                    "fillAlphas": 1,
                    "fillColors": "#3F88C5",
                    "id": "AmGraph-1",
                    "labelPosition": "inside",
                    "labelText": "[[value]]",
                    "lineThickness": 0,
                    "showBalloon": false,
                    "title": "graph 1",
                    "type": "column",
                    "valueField": "value"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "autoGridCount": false,
                    "axisAlpha": 0,
                    "axisThickness": 0,
                    "tickLength": 0,
                    "title": ""
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": localData
        } );

        function dataUpdate(data) {
            data.Data.forEach((d, i) => {
                if (i >= localData.length) {
                    localData.push({attribute: '', value: 0});
                }
                if (d.Label) {
                    var label = d.Label.split("|");
                    localData[i].attribute = label[label.length - 1];
                }
                localData[i].value = parseFloat(d.Values[0].Value);
            });
            chart.validateData();
        }
        function datasourceChange(...event) {
            // console.log(event);
        }
    };

    var def = {
        typeName: "barchartcompare",
        iconUrl: '/Scripts/app/editor/symbols/ext/Icons/custombar1.png',
        displayName: "Comparison Bar Chart",
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Multiple,
        getDefaultConfig: function() {
            var config = PV.SymValueLabelOptions.getDefaultConfig({
                DataShape: 'TimeSeries',
                Height: 300,
                Width: 300,
                // Columns: ['Value']
                DataQueryMode: PV.Extensibility.Enums.DataQueryMode.ModeSingleton
            });
            return config;
        },
        configTitle: "Bar chart config",
        visObjectType: chartVis,

    };

    PV.symbolCatalog.register(def);
})(window.PIVisualization);