(function (PV){
    function tableVis() { }

    PV.deriveVisualizationFromBase(tableVis);
    tableVis.prototype.init = function (scope, element, timeProvider) {

        let container = document.getElementById("temp-id");
        let newId = 'custom-table-' + Math.random().toString(36).substr(2, 16);
        container.id = newId;

        // Local data store so we always have labels
        let localData = [];
        scope.items = [];

        this.onDataUpdate = dataUpdate;

        // Single= and double-click handler
        scope.drillIn = function ($event, row) {
            if (!scope.layoutMode) {
                var message, command;
                if (true && !row.hasHyperlink) {
                    command = 'expandSymbol';
                    message = { symbolName: scope.symbol.Name, datasources: [row.Path], popupTrend: true };
                }

                if (message) {
                    scope.$emit(command, message);
                    $event.stopPropagation();
                }
            }
        };

        scope.startColumnEvent = function (event, colId) {
            var mouseMoveEvent = (event) => {
                scope.config.ColWidths[colId] = parseInt(scope.config.ColWidths[colId]) + event.movementX + 'px';
            };
            document.addEventListener('mousemove', mouseMoveEvent);
            var removeEvent;
            removeEvent = (event) => {
                document.removeEventListener('mousemove', mouseMoveEvent);
                document.removeEventListener('mouseup', removeEvent);
            };
            document.addEventListener('mouseup', removeEvent);
            event.stopPropagation();
        }

        function getTimeDifference() {
            return (new Date(timeProvider.getServerEndTime()).getTime() - new Date(timeProvider.getServerStartTime()).getTime()) / 1000
        }

        function dataUpdate(data) {
            if (data === null) return;
            if (data.Rows === undefined) return;

            // Copy the data into our local store.
            data.Rows.forEach((d, idx) => {

                if (localData[idx] === undefined) {
                    // Check the trend
                    let newSparkLineSegments = d.SparkLineSegments;
                    if (d.Value === "1") {
                        // We might have a flat line in the trend...
                        if (d.SparkLineSegments[0].split(" ").find(v => parseInt(v.split(",")[1])) > 0) {
                            // Not a flat line!
                        } else {
                            // console.log(d.Label, 'flat line', d.SparkLineSegments);
                            // Yup, only flat lines!
                            newSparkLineSegments[0] = newSparkLineSegments[0].split(" ").map(v => v.split(",")[0]+",100").join(" ");
                        }
                    }

                    // Initialize
                    localData[idx] = {
                        Path: d.Path,
                        Label: d.Label,
                        Value: d.Value,
                        Time: d.Time,
                        SparkLineSegments: newSparkLineSegments
                    };
                } else {
                    // Check the trend
                    let newSparkLineSegments = d.SparkLineSegments;
                    if (d.Value === "1") {
                        // We might have a flat line in the trend...
                        if (d.SparkLineSegments[0].split(" ").find(v => parseInt(v.split(",")[1])) > 0) {
                            // Not a flat line!
                        } else {
                            // console.log(d.Label, 'flat line', d.SparkLineSegments);
                            // Yup, only flat lines!
                            newSparkLineSegments[0] = newSparkLineSegments[0].split(" ").map(v => v.split(",")[0]+",100").join(" ");
                        }
                    }

                    // Update
                    localData[idx].Label = d.Label || localData[idx].Label;
                    localData[idx].Value = d.Value;
                    localData[idx].Time = d.Time;
                    localData[idx].SparkLineSegments = newSparkLineSegments;
                    localData[idx].Path = d.Path || localData[idx].Path;
                }
            });

            scope.items = localData
                .map(d => {
                    const cutLabel = d.Label.split('|');
                    const asset = cutLabel[0];
                    const category = cutLabel[1];
                    const name = cutLabel[2];

                    const onOff = parseInt(d.Value);

                    let values = [];
                    
                    let timeActive;
                    if (d.SparkLineSegments && d.SparkLineSegments[0]) {
                        values = d.SparkLineSegments[0].split(' ').map(v => v.split(','));
                        let sum = 0;
                        for (let i = 1; i < values.length; i++) {
                            sum += (values[i][0] - values[i - 1][0]) * (values[i - 1][1]);
                        }
                        timeActive = sum / (100 * 100) * getTimeDifference();
                    }

                    // Calculate active time
                    const result = {
                        name: name.split(' - ')[1],
                        category: category.split(':')[1],
                        onOff: onOff,
                        timeActive: Math.round(timeActive / 60 ),
                        Path: d.Path,
                        trend: PV.TrendUtils.buildSVGPathFromPoints(d.SparkLineSegments, parseInt(scope.config.ColWidths[4]), 30).join(''),
                        show: onOff === 1 || timeActive > 0
                    };
                    return result;
                }).sort((b, a) => {
                    if (scope.config.DoubleSort) {
                        return a.onOff - b.onOff || a.timeActive - b.timeActive;
                    }
                    return a.timeActive - b.timeActive;
                }); // First sort by onoff, then by timeactive;

            // Create view objects
            // scope.items = localData.filter(d => d.Label.split('|').length === 3)
            //     .map(d => {
            //         const cutLabel = d.Label.split('|');
            //         const asset = cutLabel[0];
            //         const category = cutLabel[1];
            //         const name = cutLabel[2];

            //         const onOff = parseInt(d.Value);

            //         // Find a matching timeactive symbol if any
            //         const listOfTimeActiveValues = localData.filter(d => d.Label.split('|').length === 4);

            //         const timeActive = listOfTimeActiveValues.find(d => d.Label.split('|')[0] == asset && d.Label.split('|')[1] == category && d.Label.split('|')[2] == name) || {Value:'N/A'};

            //         const result = {
            //             name: name.split(' - ')[1],
            //             category: category.split(':')[1],
            //             onOff: onOff,
            //             timeActive: timeActive.Value,
            //             show: onOff === 1 || timeActive.Value > 0
            //         };
            //         return result;
            //     })
            //     .sort((b, a) => {
            //         if (scope.config.DoubleSort) {
            //             return a.onOff - b.onOff || a.timeActive - b.timeActive;
            //         }
            //         return a.timeActive - b.timeActive;
            //     }); // First sort by onoff, then by timeactive
            setTimeout(updateRows, 0);
        }

        function updateRows() {
            let rows = container.children[1].children;

            // Alternating row colors
            for (let i = 0; i < rows.length; i++) {
                if (i % 2 == 0) rows.item(i).classList.add('customodd2');
                if (i % 2 == 1) rows.item(i).classList.remove('customodd2');
            }
        }
        
        function dateParse(d) {
            var splitDate = d.split(" ");
            var date = splitDate[0];
            var day = date.split("-")[0];
            var month = date.split("-")[1];
            var year = date.split("-")[2];
  
            var time = splitDate[1];
            var hour = time.split(":")[0];
            var minute = time.split(":")[1];
            var seconds = time.split(":")[2];
  
            return new Date(Date.UTC(year, month - 1, day, hour, minute, seconds));
          }
    };
    
    var def = {
        typeName: "customtable2",
        iconUrl: '/Scripts/app/editor/symbols/ext/Icons/customtable.png',
        displayName: "New custom table",
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Multiple,
        getDefaultConfig: function() {
            var config = PV.SymValueLabelOptions.getDefaultConfig({
                DataShape: 'Table',
                Columns: ['Label', 'Description', 'Value', 'Units', 'Trend', 'Minimum', 'Maximum'],
                Height: 300,
                Width: 300,
                NameWidth: 60,
                CatWidth: 30,
                OnOffWidth: 5,
                TimeWidth: 5,
                ShowAll: false,
                DoubleSort: true,
                Bold: true,
                ColWidths: ['200px', '150px', '100px', '120px', '100px'],
                HeaderText: ['Alarm name','Category','Status','Minutes active','Trend']
                // DataQueryMode: PV.Extensibility.Enums.DataQueryMode.ModeSingleton
            });
            return config;
        },
        configTitle: "Table config",
        visObjectType: tableVis,
        inject: ['timeProvider']
    };

    PV.symbolCatalog.register(def);
})(window.PIVisualization);