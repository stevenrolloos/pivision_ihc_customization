// <copyright file="PIVisualization.sym-statictext.js" company="OSIsoft, LLC">
// Copyright © 2015-2017 OSIsoft, LLC. All rights reserved.
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF OSIsoft, LLC.
// USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR EXPRESS WRITTEN
// PERMISSION OF OSIsoft, LLC.
//
// RESTRICTED RIGHTS LEGEND
// Use, duplication, or disclosure by the Government is subject to restrictions
// as set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and
// Computer Software clause at DFARS 252.227.7013
//
// OSIsoft, LLC.
// 1600 Alvarado Street, San Leandro, CA 94577
// </copyright>

window.PIVisualization = window.PIVisualization || {};
window.PIVisualization.ClientSettings = window.PIVisualization.ClientSettings || {};

(function (PV) {
    'use strict';

    function textVis() { }
    PV.deriveVisualizationFromBase(textVis);

    textVis.prototype.init = function (scope, elem, $rootScope) {
        this.onConfigChange = configChange;
        this.scope.runtimeData.position.autoSizeWidth = resize;
        this.scope.runtimeData.position.customSyncHandler = PV.TextResize.adjustIntegerFont;

        var sizingLine = elem.find('.text-symbol-sizing-line');

        Object.defineProperty(scope, 'displayText', {
            get: function () {
                return scope.config.SetTextFromLink ? 
                    (scope.config.LinkURL ? scope.config.LinkURL : PV.ResourceStrings.StaticTextEmptyLinkKeyword) :
                    (scope.runtimeData.isEditing ? scope.runtimeData.StaticText : scope.config.StaticText);
            }
        });

        function configChange(newConfig, oldConfig) {
            if (newConfig.FontSize !== oldConfig.FontSize || newConfig.Height !== oldConfig.Height) {
                resize();
            }
        }

        function resize() {
            elem.find('.static-text-symbol').css('font-size', PV.TextResize.calculateFontSize(scope.runtimeData, scope.config));
            elem.find('span')[0].innerText = scope.displayText;
            scope.runtimeData.position._w = sizingLine.width();

            // Notify collection
            scope.$emit('autoSizeWidthCompleted');
        };

        scope.clickLinkURL = function () {
            if (scope.config.LinkURL && !scope.layoutMode) {
                $rootScope.$broadcast('expandSymbol', { symbolName: scope.runtimeData.name });
            }
        };

        // open config when created and selected (excludes undo/redo)
        if (scope.runtimeData.isSelected) {
            $rootScope.$broadcast('showConfigPane', { mode: "default", title: PV.ResourceStrings.FormatTextOption });
        }

        scope.$watchGroup(['config.StaticText', 'config.LinkURL', 'config.SetTextFromLink'], function(nv, ov) {
            if (nv[0] !== ov[0]) {
                validateConfig(scope.config);
                resize();
            }

            if (scope.config.SetTextFromLink) {
                scope.runtimeData.StaticText = scope.config.LinkURL ? scope.config.LinkURL : PV.ResourceStrings.StaticTextEmptyLinkKeyword;
            } else {
                scope.runtimeData.StaticText = scope.config.StaticText;
            }
        });

        scope.$watch('runtimeData.StaticText', function (nv, ov) {
            // changed when editing in config pane or when Static Text is synced with LinkURL
            if (scope.runtimeData.StaticText && scope.runtimeData.StaticText.length > PV.ClientSettings.MaxTextSymbolInput) {
                scope.runtimeData.StaticText = scope.runtimeData.StaticText.substr(0, PV.ClientSettings.MaxTextSymbolInput);
            }
            
            if (nv !== ov) {
                resize();
            }
        });
    };

    function validateConfig(config) {
        if (config) {
            // Existing symbols may default to integer font size to avoid changing the layout of older displays, but switch to decimal sizes when height changes
            if (config.UseIntegerFontSize === undefined) {
                config.UseIntegerFontSize = true;
            }

            if (config.Width !== undefined) {
                delete config.Width;
            }
            if (config.StaticText && config.StaticText.length > PV.ClientSettings.MaxTextSymbolInput) {
                config.StaticText = config.StaticText.substr(0, PV.ClientSettings.MaxTextSymbolInput);
            }
            if (config.LinkURL && config.LinkURL.length > PV.ClientSettings.MaxTextSymbolInput) {
                config.LinkURL = '';
            }
            // CS 2016 R2 and previous versions used ['Fill', 'Blink']
            if (config.Multistates && config.Multistates[0]) {
                config.Multistates[0].StateVariables[0] = 'MSColor';
                config.Multistates[0].StateVariables[1] = 'MSBlink';
            }
        }
        return true;
    }

    function keyPress(evt) {
        var key = evt.which || evt.keyCode || 0;
        if (key === 13) { 
            evt.currentTarget.blur();
        }
    }

    function onBlur(config, rt) {
        if (!rt.StaticText || rt.StaticText.length === 0) {
            config.StaticText = PV.ResourceStrings.StaticTextEmptyKeyword;
            rt.StaticText = PV.ResourceStrings.StaticTextEmptyKeyword;
        } else {
            config.StaticText = rt.StaticText;
        }

        rt.isEditing = false;
        rt.position.syncConfig = true;
    }

    function onFocus(rt) {
        rt.isEditing = true;
        rt.position.syncConfig = false;
    }

    function onConfigPaneChange(config, rt) {
        // when changing or closing the config pane while input box is in focus, we need to be sure blur event is fired
        if (rt.isEditing) {
            onBlur(config, rt);
        }
    }

    function getDefaultConfig() {
        return {
            Bold: false,
            IHCFont: false,
            Height: 34.5,
            SetTextFromLink: false,
            StaticText: PV.ResourceStrings.StaticTextEmptyKeyword,
            LinkURL: '',
            NewTab: false,
            Fill: 'rgba(255,255,255,0)',
            Stroke: 'rgba(255,255,255,1)',
            Rotation: 0,
            FontSize: null,
            UseIntegerFontSize: false
        };
    }

    // When config panes opens for a new symbol, set focus on text input field and select
    function configInit(scope) {
        if (scope.config.StaticText === PV.ResourceStrings.StaticTextEmptyKeyword) {
            setTimeout(function () {
                var inputText = $('input[ng-model$="StaticText"]');
                inputText.focus();
                setTimeout(function () { inputText.select(); }, 100);
            }, 100);
        }

        scope.fontResizeCallback = PV.TextResize.setPositionFromFont;
    }

    var def = {
        typeName: 'customtext',
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.None,
        createSymbolOnDataSourceDrop: true,
        iconUrl: 'Images/chrome.text.svg',
        getDefaultConfig: getDefaultConfig,
        themes: { reverse: { Stroke: 'black' } },
        inject: ['$rootScope'],
        StateVariables: ['MSColor', 'MSBlink'],
        resizerMode: 'AutoWidth',
        loadConfig: validateConfig,
        formatMap: { TextResize: 'TextResize' },
        visObjectType: textVis,
        supportsCollections: true,

        configInit: configInit,
        configure: {
            keyPress: keyPress,
            onBlur: onBlur,
            onConfigPaneChange: onConfigPaneChange,
            onFocus: onFocus,
        },
        configTitle: PV.ResourceStrings.FormatTextOption
    };

    PV.symbolCatalog.register(def);

})(window.PIVisualization);
