// <copyright file="PIVisualization.sym-value.js" company="OSIsoft, LLC">
// Copyright © 2014-2017 OSIsoft, LLC. All rights reserved.
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF OSIsoft, LLC.
// USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR EXPRESS WRITTEN
// PERMISSION OF OSIsoft, LLC.
//
// RESTRICTED RIGHTS LEGEND
// Use, duplication, or disclosure by the Government is subject to restrictions
// as set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and
// Computer Software clause at DFARS 252.227.7013
//
// OSIsoft, LLC.
// 1600 Alvarado Street, San Leandro, CA 94577
// </copyright>

/// <reference path="../_references.js" />

window.PIVisualization = window.PIVisualization || {};
window.PIVisualization.ClientSettings = window.PIVisualization.ClientSettings || {};

(function (PV) {
    'use strict';    
    function valVis() { }
    PV.deriveVisualizationFromBase(valVis);

    valVis.prototype.init = function (scope, elem, symValueLabelOptions) {
        this.onConfigChange = configChange;
        this.onDataUpdate = dataUpdate;
        this.scope.runtimeData.position.autoSizeWidth = resize;
        this.scope.runtimeData.position.customSyncHandler = PV.TextResize.adjustIntegerFont;

        scope.symTime = '...';
        scope.symValue = '...';
        scope.valueMetaData = { Units: '' };

        symValueLabelOptions.init(scope);

        var fontMetrics = scope.def.fontMetrics;
        var indicatorFontMultiplier = 0.75;
        var indicatorPadding = 8;
        var defaultIndicatorPadding = 2;
        scope.differentialText = '...';
        scope.IndicatorFill = scope.config.Stroke;
        scope.targetText = '...';
        
        scope.indicatorPosition = {
            left: undefined,
            width: undefined,
            points: '0,0 0,0 0,0',
            alternateStatePath: ''
        };        

        var allLines = elem.find('.text-symbol-sizing-line');
        var lineText = [];
        var direction;
        var diffValue = '...';
        var diffPercent = '...';
        var fontSizingObject = {};

        Object.defineProperty(fontSizingObject, 'diffWidth', {
            get: function() {
                return scope.config.ShowDifferential ? 1.1 * fontMetrics.charWidth * scope.indicatorFontSize / 12 * scope.differentialText.toString().length : 0;
            }
        });

        Object.defineProperty(fontSizingObject, 'targetWidth', {
            get: function() {
                return scope.config.ShowTarget ? 1.1 * fontMetrics.charWidth * scope.indicatorFontSize / 12 * scope.targetText.toString().length : 0;
            }
        });

        function setupResizingLines() {
            scope.runtimeData.shownLines = [];
            lineText = [];
            if (scope.config.ShowLabel) {
                scope.runtimeData.shownLines.push(allLines[0]);
                lineText.push(scope.runtimeData.valueLabel ? scope.runtimeData.valueLabel.displayName : '');
            }
            if (scope.config.ShowValue) {
                scope.runtimeData.shownLines.push(allLines[1]);
                lineText.push(scope.symValue + (scope.valueMetaData.Units ? '\xa0' + scope.valueMetaData.Units : ''));
            }
            if (scope.config.ShowTime) {
                scope.runtimeData.shownLines.push(allLines[2]);
                lineText.push(scope.symTime);
            }
        }

        setupResizingLines();
        resize(true);

        scope.runtimeData.onDisplayNameChanged = function () {
            resize(true);
        };

        scope.openHyperlink = function () {
            if (scope.isLink && !scope.layoutMode) {
                scope.$emit('openHyperlink', { URL: scope.symValue, newTab: true });
            }
        };

        function dataUpdate(data) {
            var valueLabel = scope.runtimeData.valueLabel;

            if (data && (scope.symTime !== data.Time ||
                        scope.symValue !== data.Value ||
                        (data.Description && valueLabel.description !== data.Description) ||
                        (data.Label && valueLabel.label !== data.Label) ||
                        (data.Path && valueLabel.path !== data.Path.slice(3)) ||
                        (data.TargetData && data.TargetData.Target !== scope.target)
                        )) {

                scope.symTime = data.Time;
                scope.symValue = data.Value;
                scope.isLink = PV.navLinkWhitelistRegExp.test(scope.symValue);

                // Metadata received on first update, periodically afterward
                if (data.Label) {
                    symValueLabelOptions.dataUpdate(scope, data);
                    scope.valueMetaData = { Path: PV.Utils.parseTooltip(data.Path), Units: data.Units || '' };
                }

                if (data.TargetData) {
                    if (data.TargetData.TargetErrorCode) {
                        scope.target = '...';
                        scope.targetText = '...';
                        direction = PV.ResourceStrings.Error;
                        diffValue = '...';
                        diffPercent = '...';
                    }

                    if (data.TargetData.Target) {
                        scope.target = data.TargetData.Target;
                        scope.targetText = PV.ResourceStrings.TooltipTarget.replace('{0}', scope.target);
                    }

                    if (data.TargetData.TargetDirection) {
                        direction = data.TargetData.TargetDirection;
                    }

                    if(data.TargetData.TargetDiffValue) {
                        diffValue = data.TargetData.TargetDiffValue;
                    }
                        
                    if (data.TargetData.TargetDiffPercent) {
                        diffPercent = data.TargetData.TargetDiffPercent;
                    }

                    scope.diffValue = diffValue;
                    scope.diffPercent = diffPercent;
                }
                
                scope.tooltip = PV.Utils.generateTooltip(scope);
                resize(true);
            }
        }

        function needResize(newConfig, oldConfig) {
            // only call resize() on changes in config properties that can cause width adjustment
            return ['DifferentialType', 'FontSize', 'Height', 'NameType', 'ShowDifferential', 'ShowIndicator', 'ShowLabel', 'ShowTarget', 'ShowTime', 'ShowUOM', 'ShowValue'].some(function (prop) {
                return newConfig[prop] !== oldConfig[prop];
            });
        }

        function configChange(newConfig, oldConfig) {
            if (needResize(newConfig, oldConfig)) {
                resize(false, newConfig.ShowLabel !== oldConfig.ShowLabel, newConfig.ShowValue !== oldConfig.ShowValue, newConfig.ShowUOM !== oldConfig.ShowUOM, newConfig.ShowTime !== oldConfig.ShowTime);
            }

            if (!!newConfig.ShowIndicator && !oldConfig.ShowIndicator) {
                scope.$emit('refreshDataForChangedSymbols');
            }
        }

        function resize(contentChange, showNameChange, showValueChange, showUnitsChange, showTimeChange) {
            var oldShownLines = scope.runtimeData.shownLines.length;
            setupResizingLines();
            var rt = scope.runtimeData;

            // If the content or hide/show properties change, bind values directly in DOM rather than use ng-bind, which requires another digest cycle to determine width
            if (contentChange) {
                allLines[0].firstElementChild.innerText = rt.valueLabel ? rt.valueLabel.displayName : '';
                allLines[1].children[0].innerText = scope.symValue;
                allLines[1].children[1].innerText = scope.valueMetaData.Units ? '\xa0' + scope.valueMetaData.Units : '';
                allLines[2].firstElementChild.innerText = scope.symTime;
            }

            if (showNameChange) {
                    allLines[0].classList.contains('ng-hide') ? $(allLines[0]).removeClass('ng-hide') : $(allLines[0]).addClass('ng-hide');
            } else if (showValueChange) {
                    allLines[1].classList.contains('ng-hide') ? $(allLines[1]).removeClass('ng-hide') : $(allLines[1]).addClass('ng-hide');
            } else if (showUnitsChange) {
                    allLines[1].children[1].classList.contains('ng-hide') ? $(allLines[1].children[1]).removeClass('ng-hide') : $(allLines[1].children[1]).addClass('ng-hide');
            } else if (showTimeChange) {
                    allLines[2].classList.contains('ng-hide') ? $(allLines[2]).removeClass('ng-hide') : $(allLines[2]).addClass('ng-hide');
            }

            // Adjust font size and calculate new width
            var w = 0;
            if (rt.shownLines.length > 0) {
                // When hiding/showing a line, do not change the font size. (Special case: when only target indicator is shown and then a line is shown, do set the font size.)
                var newFontSize = PV.TextResize.calculateFontSize(scope.runtimeData, scope.config);
                if (!(showNameChange || showValueChange || showTimeChange) || oldShownLines === 0) {
                    $(rt.shownLines[0].parentElement).css('font-size', newFontSize);
                }

                rt.shownLines.forEach(function (e, index) {
                    var newW;
                    if (lineText[index] === '...') {
                        // on load, avoid expensive DOM operations by estimating width of ellipsis (calculation determined heuristically)
                        newW = 0.83 * newFontSize + 0.09;
                    } else {
                        var children = $(e).children();
                        newW = children.length === 1 || !scope.config.ShowUOM ? children[0].offsetWidth : children[0].offsetWidth + children[1].offsetWidth;
                    }

                    w = newW > w ? newW : w;
                });
            }

            var widthChange = rt.position._w !== w;
            rt.position._w = w;

            if (scope.config.ShowIndicator) {
                // Adjust symbol with indicator
                adjustFontWidthForIndicator();
            }

            // Notify collection
            if (widthChange) {
                scope.$emit('autoSizeWidthCompleted');
            }
        }

        function adjustFontWidthForIndicator() {

            if (scope.config.DifferentialType === 'percent') {
                scope.differentialText = diffPercent;
            } else if (scope.config.DifferentialType === 'customvalue') {
                scope.differentialText = diffValue;
            }

            var height;
            var fontHeight;
            var addPadding = false;
            indicatorFontMultiplier = .75;

            if (scope.runtimeData.shownLines.length) {
                height = $(scope.runtimeData.shownLines[0]).height();
                fontHeight = height * indicatorFontMultiplier;
                addPadding = true;
            } else {
                indicatorFontMultiplier = 1;
                height = scope.position.height;
                fontHeight = scope.position.height;
            }

            var top;
            if (scope.config.ShowTarget) {
                if (height > scope.position.height / 2) {
                    height = scope.position.height / 2;
                }

                if (fontHeight > scope.position.height / 2 * indicatorFontMultiplier) {
                    fontHeight = scope.position.height / 2 * indicatorFontMultiplier;
                }

                top = scope.position.height / 4 - height / 2;
            }
            else {
                top = scope.position.height / 2 - height / 2;
            }
            
            scope.indicatorFontSize = 12 * fontHeight / (1.35 * fontMetrics.charHeight);
            var availableWidth = Math.max(height + (fontSizingObject.diffWidth), (fontSizingObject.targetWidth));
            scope.runtimeData.position.width += availableWidth + (addPadding ? indicatorPadding : defaultIndicatorPadding);
            updateIndicator(height, fontHeight, top, availableWidth);
        }

        function updateIndicator(height, fontHeight, top, availableWidth) {
            scope.indicatorPosition = {
                width: height,
                height: height,
                top: top
            };
                
            var leftStart = scope.position.width - availableWidth;
            var topGreaterThanBottom = false;
            if (scope.indicatorPosition.width + fontSizingObject.diffWidth >= fontSizingObject.targetWidth) {
                topGreaterThanBottom = true;
            }

            if (scope.config.ShowDifferential) {
                scope.differentialPosition = {
                    width: fontSizingObject.diffWidth,
                    height: fontHeight
                };

                var diffTop = scope.indicatorPosition.top + scope.indicatorPosition.height / 2 - scope.differentialPosition.height / 2;
                scope.differentialPosition.top = diffTop < 0 ? 0 : diffTop;

                if (topGreaterThanBottom) {
                    scope.indicatorPosition.left = leftStart;
                    scope.differentialPosition.left = leftStart + scope.indicatorPosition.width;
                } else {
                    scope.indicatorPosition.left = scope.position.width - availableWidth * 0.75 - scope.indicatorPosition.width / 2;
                    scope.differentialPosition.left = scope.position.width - availableWidth * 0.25 - scope.differentialPosition.width / 2;
                }

                if (scope.differentialPosition.left + scope.differentialPosition.width > scope.position.width) {
                    scope.differentialPosition.left = scope.position.width - scope.differentialPosition.width;
                }

            } else {
                scope.indicatorPosition.left = scope.position.width - availableWidth / 2 - scope.indicatorPosition.width / 2;
            }

            generateIndicatorPoints(direction);

            if (scope.config.ShowTarget) {
                scope.targetPosition = {
                    left: scope.position.width - availableWidth,
                    width: fontSizingObject.targetWidth,
                    height: fontHeight,
                    top: scope.position.height * 0.75 - fontHeight / 2
                };

                if (topGreaterThanBottom) {
                    scope.targetPosition.left = scope.position.width - availableWidth / 2 - scope.targetPosition.width / 2;
                }
            }
            
        }

        function generateIndicatorPoints(direction) {
            if (direction) {
                scope.IndicatorFill = scope.config.IndicatorFillNeutral;
                var center = scope.indicatorPosition.width / 2;
                var point1X, point1Y,
                    point2X, point2Y,
                    point3X, point3Y;

                point1X = 0;
                point2X = center;
                point3X = scope.indicatorPosition.width;
                if (direction === 'Up' || direction === 'Down') {

                    if (direction === 'Up') {
                        point1Y = scope.indicatorPosition.height;
                        point3Y = scope.indicatorPosition.height;
                        point2Y = 0;
                        scope.IndicatorFill = scope.config.IndicatorFillUp;
                    } else if (direction === 'Down') {
                        point1Y = 0;
                        point3Y = 0;
                        point2Y = scope.indicatorPosition.height;
                        scope.IndicatorFill = scope.config.IndicatorFillDown;
                    }

                    scope.indicatorPosition.points = point1X + ',' + point1Y + ' ' + point2X + ',' + point2Y + ' ' + point3X + ',' + point3Y;
                    scope.indicatorPosition.alternateStatePath = '';
                } else if (direction === 'Neutral') {
                    scope.indicatorPosition.alternateStatePath = 'M' + point1X + ' ' + (scope.indicatorPosition.height / 2) + ' L' + point3X + ' ' + (scope.indicatorPosition.height / 2);
                    scope.indicatorPosition.points = '';
                } else if (direction === 'Error') {
                    scope.indicatorPosition.alternateStatePath = 'M' + point1X + ' ' + 0 + ' L' + point3X + ' ' + scope.indicatorPosition.height;
                    scope.indicatorPosition.alternateStatePath += 'M' + point1X + ' ' + scope.indicatorPosition.height + ' L' + point3X + ' ' + 0;
                    scope.indicatorPosition.points = '';
                }
            }            
        }
    };

    function setAlignment(rt, config) {
        // Update the left coordinate, which is used in the rt.position to calculate the new anchor point, but could have changed (via hide/show lines or text resize, for example)
        if (rt.position._c && !rt.position._rt) { 
            rt.position.left = rt.position._c - rt.position.width / 2; // previous alignment was center
        } else if (rt.position._rt && !rt.position._c) { 
            rt.position.left = rt.position._rt - rt.position.width; // previous alignment was right
        }

        // Reset the anchor position
        if (config.TextAlignment === 'right') {
            rt.position.right = rt.position.left + rt.position.width;
            rt.position.center = undefined;
        } else if (config.TextAlignment ===  'center') {
            rt.position.center = rt.position.left + rt.position.width / 2;
            rt.position.right = undefined;
        } else {
            rt.position.right = undefined;
            rt.position.center = undefined;
        }
    }

    function updateHeight(rt, config) {
        var shownLines = [config.ShowLabel, config.ShowValue, config.ShowTime].filter(function (e) { return e; }).length;

        if (shownLines > 0 && rt.shownLines.length > 0) {
            var height = Math.round(100 * rt.position.height * shownLines / rt.shownLines.length) / 100;

            // Bypass the position.height setter since it invokes the autoSizeWidth handler. This the handler is already called via configChange.
            rt.position._h = height;
            config.Height = height;
        }
    }

    function loadConfig(config) {
        if (config) {
            // Existing symbols may default to integer font size to avoid changing the layout of older displays, but switch to decimal sizes when height changes
            if (config.UseIntegerFontSize === undefined) {
                config.UseIntegerFontSize = true;
            }

            if (config.Width !== undefined) {
                delete config.Width;
            }

            if (config.CustomName && config.CustomName.length > PV.ClientSettings.MaxTextSymbolInput) {
                config.CustomName = config.CustomName.substr(0, PV.ClientSettings.MaxTextSymbolInput);
            }
        }
        return true;
    }

    function configInit(scope) {
        scope.fontResizeCallback = PV.TextResize.setPositionFromFont;
    }

    var def = {
        typeName: 'customvalue',
        displayName: PV.ResourceStrings.ValueSymbol,
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Single,
        iconUrl: 'Images/chrome.value.svg',
        getDefaultConfig: function () {
            var config = PV.SymValueLabelOptions.getDefaultConfig({
                DataShape: 'Value',
                Height: 56.5,
                Fill: 'rgba(255,255,255,0)',
                Stroke: 'rgba(119,136,153,1)',
                ValueStroke: 'rgba(255,255,255,1)',
                ShowTime: true,
                IndicatorFillUp: 'white',
                IndicatorFillDown: 'white',
                IndicatorFillNeutral: 'gray',
                ShowDifferential: true,
                DifferentialType: 'percent',                
                ShowIndicator: false,
                ShowValue: true,
                ShowTarget: true,
                TextAlignment: 'left',
                FontSize: null,
                Bold: false,
                IHCFont: false,
                UseIntegerFontSize: false
            });
            return config;
        },
        themes: {
            reverse: {
                ValueStroke: 'black',
                IndicatorFillUp: 'black',
                IndicatorFillDown: 'black'
            }
        },
        loadConfig: loadConfig,
        configInit: configInit,
        resizerMode: 'AutoWidth',
        StateVariables: ['Fill', 'Blink'],
        inject: ['symValueLabelOptions'],
        visObjectType: valVis,
        configTitle: PV.ResourceStrings.FormatValueOption,        
        formatMap: { BackgroundColor: 'Fill', TextColor: 'Stroke', ValueColor: 'ValueStroke', TextResize: 'TextResize', Alignment: 'Alignment' },
        setAlignment: setAlignment,
        updateHeight: updateHeight,
        supportsCollections: true
    };
    def.fontMetrics = {       // Assume scale font is Ariel 12 pt
        charHeight: 10,       // Height of digit in label
        charMidHeight: 4,     // Vertical mid-point of a digit, slightly higher than half-way because of font descenders
        charWidth: 6.3,        // Average width of a character in the scale
    };

    PV.symbolCatalog.register(def);

    ////////////////////////
    // Text resize functions
    ////////////////////////
    function setPositionFromFont(rt, config) {
        if (config.FontSize) {
            rt.position.height = Math.round(100 * config.FontSize * PV.TextResize.shownLines(rt) / (PV.TextResize.descenderAdjustment * PV.TextResize.pixelsToPointsConversion)) / 100;
            PV.TextResize.adjustIntegerFont(rt.position, config, true);
        }
    }

    function heightToPoint(rt, config) {
        if (config.UseIntegerFontSize) {
            var pxSize = Math.floor(rt.position.height * PV.TextResize.descenderAdjustment / PV.TextResize.shownLines(rt));
            return Math.round(pxSize * PV.TextResize.pixelsToPointsConversion * 10) / 10;
        }

        return Math.round(rt.position.height * PV.TextResize.descenderAdjustment * PV.TextResize.pixelsToPointsConversion * 10 / PV.TextResize.shownLines(rt)) / 10;
    }

    function adjustIntegerFont(position, forceChange) {
        position = position || this;

        // On load, legacy symbols default to integer font size to avoid changing display layouts. When the height is manually changed (by resize 
        // handlers or config pane), the symbol allows non integer sizing. This config change is bundled with the height change to avoid an undo state
        if (forceChange || position._config.Height !== position._h) {
            position._config.UseIntegerFontSize = false;
        }

        // If height was changed manually, remove user specified font size
        if (position._config.Height !== position._h) {
            delete position._config.FontSize;
        }
    }

    function calculateFontSize(rt, config) {
        // If symbol is being resized manually, capture font size from current height
        if (!rt.position.syncConfig) {
            return Math.round((rt.position.height / PV.TextResize.shownLines(rt)) * PV.TextResize.descenderAdjustment * 10) / 10;
        }

        // If user has set a font size in the config pane, use it
        if (config.FontSize) {
            return config.FontSize / PV.TextResize.pixelsToPointsConversion;
        }

        // Existing symbols may default to integer font size to avoid changing the layout of older displays, but switch to decimal sizes when height changes
        return !config.UseIntegerFontSize ?
            Math.round((rt.position.height / PV.TextResize.shownLines(rt)) * PV.TextResize.descenderAdjustment * 10) / 10 :
            Math.floor((rt.position.height / PV.TextResize.shownLines(rt)) * PV.TextResize.descenderAdjustment);
    }

    function shownLines(rt) {
        return rt.shownLines && rt.shownLines.length > 0 ? rt.shownLines.length : 1;
    }

    PV.TextResize = {
        adjustIntegerFont: adjustIntegerFont,
        calculateFontSize: calculateFontSize,
        getTextWidth: (function () {
            var canvas = document.createElement('canvas').getContext('2d');
            return function (fontSize, text) {
                canvas.font = fontSize + 'px Arial, Helvetica, sans-serif';
                return canvas.measureText(text).width;
            };
        })(),
        heightToPoint: heightToPoint,
        setPositionFromFont: setPositionFromFont,
        shownLines: shownLines,
        descenderAdjustment: 0.85,
        pixelsToPointsConversion: 0.75 // Conversion from pixels to points uses standard 96dpi: 72/96
    };

})(window.PIVisualization);
