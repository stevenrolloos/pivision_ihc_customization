(function (PV) {
    function olVis() { }
    PV.deriveVisualizationFromBase(olVis);
    function applyStyle() { }
    olVis.prototype.init = function(scope, elem, rootScope) {

        this.onDataUpdate = dataUpdate;
        this.onDatasourceChange = datasourceChange;
        this.onResize = resize;
        this.onConfigChange = function(...events) {
          // console.log(events);
        }

        var localData = [];
        var dataCopy = { Data: [] };
        
        var div = document.getElementById('map');
        div.id = Math.random().toString(36).substring(2, 16);

        var iconStyle = new ol.style.Style({
          fill: new ol.style.Fill({color: [255, 0, 0]}),
          stroke: new ol.style.Stroke({
            color: scope.config.LineColor,
            width: 3
          })
        });

        var shipPath = new ol.layer.Vector({
          source: new ol.source.Vector(),
          style: iconStyle
        });

        var shipLocation = new ol.layer.Vector({
          source: new ol.source.Vector(),
          style: function (feature) {
            // console.log(feature.get('date'));
            // return iconStyle;
            return new ol.style.Style({
              image: new ol.style.Icon({
                src: '/PIVision/Scripts/app/editor/symbols/ext/icons/point.png',
                color: scope.config.IconColor,
                anchor: [0.5, 0.9],
              }),
              fill: new ol.style.Fill({color: [255, 0, 0]}),
              text: new ol.style.Text({
                text: feature.get('date').toLocaleDateString(),
                fill: new ol.style.Fill({
                  color: scope.config.TextColor
                }),
                backgroundFill: new ol.style.Fill({
                  color: scope.config.TextBackground
                }),
                padding: [2, 2, 2, 2],
                offsetY: -40
              })
            })
          }
        });
        
        var source = new ol.source.OSM();
        var layer = new ol.layer.Tile({source: source});
        var view = new ol.View({center: [0, 0], zoom: 10});
        var map = new ol.Map({
          target: div,
          layers: [layer, shipPath, shipLocation],
          view: view
        });
        
        this.onConfigChange = function (newConfig, oldConfig) {
          iconStyle.getStroke().setColor(newConfig.LineColor);
          iconStyle.setImage(new ol.style.Icon({
            src: '/PIVision/Scripts/app/editor/symbols/ext/icons/point.png',
            color: newConfig.IconColor,
            anchor: [0.5, 0.9]
          }));
          shipPath.getSource().refresh();
          shipLocation.getSource().refresh();
        }

        function resize() {
          setTimeout(() => map.updateSize(), 200);
        }
        function dataUpdate(data) {
          // console.log(data, data === null, data.Data === null, data.Data.length);
          if (data === null) {
            return;
          }
          if (data.Data.length < 2) {
            // console.log('Not enough attributes yet', data.Data);
            return;
          }
          data.Data.forEach((d, idx) => {
            if (dataCopy.Data[idx] === undefined) {
              dataCopy.Data[idx] = {
                Label: d.Label,
                EndTime: d.EndTime,
                Path: d.Path,
                StartTime: d.StartTime,
                Values: d.Values
              };
            } else {
              dataCopy.Data[idx] = {
                Label: d.Label || dataCopy.Data[idx].Label,
                EndTime: d.EndTime || dataCopy.Data[idx].EndTime,
                Path: d.Path || dataCopy.Data[idx].Path,
                StartTime: d.StartTime || dataCopy.Data[idx].StartTime,
                Values: d.Values || dataCopy.Data[idx].Values
              };
            }
          });
          // console.log(dataCopy);
          // console.log('about to remove');
          localData = [];
          shipLocation.getSource().getFeatures().forEach(f => {
            shipLocation.getSource().removeFeature(f);
          });
          shipPath.getSource().getFeatures().forEach(f => {
            shipPath.getSource().removeFeature(f);
          });
          // console.log('wooo');
          if (dataCopy.Data[0].Values.length == 0) {
            return;
          }
          // console.log('zzaaaa', localData);
          if (localData.length === 0) {
            initializeLocalDataWith(dataCopy);
          }
          updateLocalDataWith(dataCopy);
        }
        function initializeLocalDataWith(data) {
          var coords = extractCoords(data.Data[0], data.Data[1]);
          // console.log(coords, coords.length);
          if (coords.length > 5) {
            coords = coords.slice(coords.length - 5, coords.length);
          }
          // console.log(coords);
          if (coords === undefined) {
            console.error('Coordinates undefined!', coords, data);
            return;
          }
          drawLine(coords);
          coords.forEach(c => c.featureId = drawPoint(c));
          localData = coords;
          fit(coords);
        }
        function flyTo(coords) {
          view.centerOn(ol.proj.fromLonLat([coords.lon, coords.lat]), map.getSize(), [map.getSize()[0] / 2, map.getSize()[1] / 2]);
        }
        function fit(allCoords) {
          let minX = 9999999;
          let minY = 9999999;
          let maxX = -9999999;
          let maxY = -9999999;
          for (let i = 0; i < allCoords.length; i++) {
            let coordsMercator = ol.proj.fromLonLat([allCoords[i].lon, allCoords[i].lat]);
            if (coordsMercator[0] < minX) minX = coordsMercator[0];
            if (coordsMercator[0] > maxX) maxX = coordsMercator[0];
            if (coordsMercator[1] < minY) minY = coordsMercator[1];
            if (coordsMercator[1] > maxY) maxY = coordsMercator[1];
          }
          view.fit([minX, minY, maxX, maxY]);
        }
        var firstAttr;
        var secondAttr;

        // Returns {lat: number[], lon: number[], timeStamps: Date[]}
        function extractCoords(s1, s2) {
          // Are they of the same asset?
          if (s1.Label) {
            firstAttr = s1.Label;
          }
          if (s2.Label) {
            secondAttr = s2.Label;
          }
          // console.log('First attr is: ', firstAttr);
          // console.log('Second attr is: ', secondAttr);
          if (firstAttr === undefined || secondAttr === undefined) {
            return;
          }
          // console.log('Extracting coords');
          var sameAsset = firstAttr.split('|')[0] === secondAttr.split('|')[0];
          if (sameAsset) {
            // console.log('same asset');
            // Determine lat/lon
            var bothSame = firstAttr.split('|')[1] === secondAttr.split('|')[1];
            if (!bothSame) {
              // console.log('theyre different');
              var latC = firstAttr.split('|')[1] === 'Test Latitude' ? s1.Values : s2.Values;
              var lonC = firstAttr.split('|')[1] === 'Test Longitude' ? s1.Values : s2.Values;
			  //var latC = firstAttr.split('|')[1] === 'Latitude' ? s1.Values : s2.Values;
              //var lonC = firstAttr.split('|')[1] === 'Longitude' ? s1.Values : s2.Values;
              var timeStamps = s1.Values.map(v => dateParse(v.Time));
              // console.log(timeStamps);
              var lats = latC.map(v => parseFloat(v.Value.toString().replace(",", ".")));
              var lons = lonC.map(v => parseFloat(v.Value.toString().replace(",", ".")));

              var coords = [];
              for (var i = 0; i < lats.length; i++) {
                coords.push({
                  lat: lats[i],
                  lon: lons[i],
                  timeStamp: timeStamps[i]
                });
              }
              // console.log(coords);
              return coords;
            }
            console.error('The two attributes are the same');
          } else {
            console.error('Sources are from different assets');
          }
          return undefined;
        }
        function dateParse(d) {
          // console.log(d);
          var splitDate = d.split(" ");
          var date = splitDate[0];

          var day, month, year;
          if (date.split('/').length === 3){
            day = date.split("/")[1];
            month = date.split("/")[0];
            year = date.split("/")[2];
          } else {
            day = date.split("-")[0];
            month = date.split("-")[1];
            year = date.split("-")[2];
          }

          var time = splitDate[1];
          var hour = time.split(":")[0];
          var minute = time.split(":")[1];
          var seconds = time.split(":")[2];

          return new Date(Date.UTC(year, month - 1, day, hour, minute, seconds));
		  console.error('abc ', date);
		  
        }
        var line;
        function drawLine(coords) {
          // console.log('drawing line');
          lineC = coords.map(v => ol.proj.fromLonLat([v.lon, v.lat]));
          line = new ol.geom.LineString(lineC);
          shipPath.getSource().addFeature(new ol.Feature(line));
        }
        function drawPoint(c) {
          var point = new ol.geom.Point(ol.proj.fromLonLat([c.lon, c.lat]));
          let feature  = new ol.Feature(point);
          feature.set('date', c.timeStamp);
          feature.setId(parseInt(Math.random() * Date.now()));
          shipLocation.getSource().addFeature(feature);
          return feature.getId();
        }
        function updateLocalDataWith(data) {
          var coords = extractCoords(data.Data[0], data.Data[1]);
          if (coords.length > 5) {
            coords = coords.slice(coords.length - 5, coords.length);
          }
          if (coords === undefined) {
            console.error('Unable to perform update');
          }
          var lastCoords = coords[coords.length - 1];

          if (hasPositionChanged(lastCoords)) {
            localData.push(lastCoords);
            if(localData.length > 5) {
              localData.reverse();
              let feature = shipLocation.getSource().getFeatureById(localData.pop().featureId);
              shipLocation.getSource().removeFeature(feature);
              localData.reverse();
            }
            localData[localData.length - 1].featureId = drawPoint(lastCoords);
            line.setCoordinates(localData.map(c => [c.lon, c.lat]));
            // appendPointToLine(lastCoords);
            return;
          }
          // console.log('Position has not been changed');
        }
        function hasPositionChanged(coord) {
          return !(coord.lat === localData[localData.length - 1].lat &&
                coord.lon === localData[localData.length - 1].lon);
        }
        // function appendPointToLine(c) {
        //   line.appendCoordinate(ol.proj.fromLonLat([c.lon, c.lat]));
        // }
        function datasourceChange(...event) {

        }
    };

    var def = {
        typeName: "olmap",
        iconUrl: '/Scripts/app/editor/symbols/ext/Icons/mapsingle.png',
        displayName: "Openlayers",
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Multiple,
        getDefaultConfig: function() {
            var config = PV.SymValueLabelOptions.getDefaultConfig({
                DataShape: 'TimeSeries',
                Height: 300,
                Width: 300,
                LineColor: 'rgba(255, 0, 0, 1)',
                IconColor: 'rgba(255, 0, 0, 1)',
                TextColor: 'rgba(255, 255, 255, 1)',
                TextBackground: 'rgba(255, 0, 0, 1)',
                DataQueryMode: PV.Extensibility.Enums.DataQueryMode.ModeEvents
            });
            return config;
        },
        configTitle: "Openlayers config",
        visObjectType: olVis,

    };

    PV.symbolCatalog.register(def);
})(window.PIVisualization);