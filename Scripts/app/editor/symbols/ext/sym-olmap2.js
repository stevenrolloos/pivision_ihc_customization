(function (PV) {
    function olVis() { }
    PV.deriveVisualizationFromBase(olVis);

    olVis.prototype.init = function(scope, elem, rootScope) {
        // console.log('Initialized!');

        this.onDataUpdate = dataUpdate;
        this.onDatasourceChange = datasourceChange;
        this.onResize = resize;

        var localData = [];
        
        var div = document.getElementById('map');
        div.id = Math.random().toString(36).substring(2, 16);

        var shipLayer = new ol.layer.Vector({
          source: new ol.source.Vector(),
          style: function(feature) {
            // console.log(feature.get('asset'));
            return  new ol.style.Style({
              image: new ol.style.Icon({
                src: '/PIVision/Scripts/app/editor/symbols/ext/icons/point.png',
                color: scope.config.IconColor,
                anchor: [0.5, 0.9],
              }),
              fill: new ol.style.Fill({color: scope.config.IconColor}),
              stroke: new ol.style.Stroke({
                color: scope.config.IconColor,
                width: 3
              }),
              text: new ol.style.Text({
                text: feature.get('asset').asset,
                fill: new ol.style.Fill({
                  color: scope.config.TextColor
                }),
                backgroundFill: new ol.style.Fill({
                  color: scope.config.TextBackground
                }),
                padding: [2, 2, 2, 2],
                offsetY: -40
              })
            });
          }
        });
        
        var source = new ol.source.OSM();
        var layer = new ol.layer.Tile({source: source});
        var view = new ol.View({center: [0, 0], zoom: 4});
        var map = new ol.Map({
          target: div,
          layers: [layer, shipLayer],
          view: view
        });
        this.onConfigChange = function (newConfig, oldConfig) {
          // iconStyle.getStroke().setColor(newConfig.LineColor);
          // iconStyle.setImage(new ol.style.Icon({
          //   src: '/PIVision/Scripts/app/editor/symbols/ext/icons/point.png',
          //   color: newConfig.IconColor,
          //   anchor: [0.5, 0.9]
          // }));
          shipLayer.getSource().refresh();
        }
        function resize() {
          setTimeout(() => map.updateSize(), 200);
        }
        function dataUpdate(data) {
          if (data === null) {
            return;
          }
          if (data.Data.length < 2) {
            return;
          }
          // console.log(data.Data.length, localData.length, localData);
          if (data.Data.length > 2 * localData.length + 1) {
            initializeLocalDataWith(data);
          }
          updateLocalDataWith(data);
        }
        function initializeLocalDataWith(data) {
          for (let i = 0; i < data.Data.length; i++) {
            labels[i] = data.Data[i].Label || labels[i] || undefined;
          }

          let minX = 9999999;
          let minY = 9999999;
          let maxX = -9999999;
          let maxY = -9999999;

          for (let i = 0; i < data.Data.length; i += 2) {
            if (localData[i>>1] !== undefined) {
              console.log('Already defined');
              continue;
            }
            // Pair value defined?
            if (data.Data[i+1] === undefined) {
              break;
            }

            // Same asset?
            var sameAsset = labels[i].split('|')[0] === labels[i+1].split('|')[0];
            if (!sameAsset) console.error('Not same asset');

            // Same attribute?
            var sameAttribute = labels[i].split('|')[1] === labels[i+1].split('|')[1];
            if (sameAttribute) console.error('Same attribute!');

            // Which is which?
            var first = labels[i].split('|')[1];
            var second = labels[i+1].split('|')[1];

            var latIndex = first === 'Test Latitude' ? i : i + 1;
            var lonIndex = latIndex === i ? i + 1 : i;

            var lat = first === 'Test Latitude' ? parseFloat(data.Data[i].Values[0].Value.replace(",", ".")) : parseFloat(data.Data[i+1].Values[0].Value.replace(",", "."));
            var lon = second === 'Test Longitude' ? parseFloat(data.Data[i+1].Values[0].Value.replace(",", ".")) : parseFloat(data.Data[i].Values[0].Value.replace(",", "."));
            
            let coordsMercator = ol.proj.fromLonLat([lon, lat]);
            var feature = new ol.Feature(new ol.geom.Point(coordsMercator));
            shipLayer.getSource().addFeature(feature);
            
            var save = {latIndex: latIndex, lonIndex: lonIndex, lat: lat, lon: lon, timeStamp: dateParse(data.Data[i].Values[0].Time), asset: labels[i].split('|')[0], feature:feature};
            feature.set('asset', save);
            localData.push(save);

          }

          for(let i = 0; i < localData.length; i++) {
            let coordsMercator = ol.proj.fromLonLat([localData[i].lon, localData[i].lat]);
            if (coordsMercator[0] < minX) minX = coordsMercator[0];
            if (coordsMercator[0] > maxX) maxX = coordsMercator[0];
            if (coordsMercator[1] < minY) minY = coordsMercator[1];
            if (coordsMercator[1] > maxY) maxY = coordsMercator[1];
          }

          view.fit([minX, minY, maxX, maxY]);
        }
        var labels = [];
        var line;
        function dateParse(d) {
          var splitDate = d.split(" ");
          var date = splitDate[0];
          var day = date.split("-")[0];
          var month = date.split("-")[1];
          var year = date.split("-")[2];

          var time = splitDate[1];
          var hour = time.split(":")[0];
          var minute = time.split(":")[1];
          var seconds = time.split(":")[2];

          return new Date(Date.UTC(year, month - 1, day, hour, minute, seconds));
        }
        function updateLocalDataWith(data) {
          localData.forEach(d => {
            d.lon = parseFloat(data.Data[d.lonIndex].Values[0].Value.replace(",", "."));
            d.lat = parseFloat(data.Data[d.latIndex].Values[0].Value.replace(",", "."));
            d.timeStamp = dateParse(data.Data[d.lonIndex].Values[0].Time);
            let newCoords = ol.proj.fromLonLat([d.lon, d.lat]);
            d.feature.getGeometry().setCoordinates(newCoords);
          });
        }
        function datasourceChange(...event) {

        }
    };

    var def = {
        typeName: "olmap2",
        iconUrl: '/Scripts/app/editor/symbols/ext/Icons/mapmulti.png',
        displayName: "Openlayers Overview Map",
        datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Multiple,
        getDefaultConfig: function() {
            var config = PV.SymValueLabelOptions.getDefaultConfig({
                DataShape: 'TimeSeries',
                Height: 300,
                Width: 300,
                LineColor: 'rgba(255, 0, 0, 1)',
                IconColor: 'rgba(255, 0, 0, 1)',
                TextColor: 'rgba(255, 255, 255, 1)',
                TextBackground: 'rgba(255, 0, 0, 1)',
                DataQueryMode: PV.Extensibility.Enums.DataQueryMode.Singleton
            });
            return config;
        },
        configTitle: "Openlayers config",
        visObjectType: olVis,

    };

    PV.symbolCatalog.register(def);
})(window.PIVisualization);